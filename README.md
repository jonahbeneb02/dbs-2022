# Datenbanksysteme (SoSe 22)
![data/Literature-data_TU-Darmstadt.csv](data/Literature-data_TU-Darmstadt.csv): beinhaltet den von der Aufgabe vorgegebenen Datensatz in Form einer [CSV-Datei](https://de.wikipedia.org/wiki/CSV_(Dateiformat))

![data/urls.csv](data/Literature-data_TU-Darmstadt.csv): Zuordnung von DOIs auf URLs

doiresolve.py: Öffnet Firefox um zu jedem [DOI](https://de.wikipedia.org/wiki/Digital_Object_Identifier) (aus dois.txt) die URL (in urls.txt) zu speichern

src\: beinhaltet eine [Flask](https://de.wikipedia.org/wiki/Flask)-Webanwendung

## Datenbank erstellen

Postgres-Shell öffnen (abweichend unter macOS)
```sudo -u postgres psql```

```CREATE USER project WITH PASSWORD '1234';```

```CREATE DATABASE literature;```

## Daten importieren
Zu aller erst müssen die tabellen erstellt werden, dafür kann der SQL code in [setup/up.sql](setup/up.sql) eingefügt werden.

Die Daten müssen für den Postgres-Benutzer lesbar sein, eine einfache Lösung ist es die daten einfach in dessen Home-Verzeichnis zu kopieren.

Literature data importieren:
```
COPY research_literature_data(source, author, title, year, journal, type, id_prefix, id, read, empirisch, exclusion_point, exclusion_reason, comment, field, topic, HMD, interaction_device, participants, abstraction_low, abstraction_medium, abstraction_high, re_exactly, re_similar, re_different, training,  walking,  balance, navigation, searching, reaching, object_interaction, memory_tasks, spatial_perception, quantitative, qualitative, experiment, secondary_research, subjective_feedback, consistent, better, worse)
FROM '/var/lib/postgresql/Literature-data_TU-Darmstadt.csv'
DELIMITER ',' CSV HEADER;
```

Urls importieren
```
COPY url(id_prefix, id, url) FROM '/var/lib/postgresql/urls.csv' DELIMITER ',' CSV HEADER;
```

Publisher-Daten importieren
```
COPY publishers(id_prefix, name) FROM '/var/lib/postgresql/doi-prefix-publishers.csv' DELIMITER ',' CSV HEADER;
```

```
ALTER TABLE publisher OWNER TO project;
ALTER TABLE research_literature_data OWNER TO project;
ALTER TABLE url OWNER TO project;
```

## Abhängigkeiten installieren
```
pip install -r requirements.txt
```

## Webserver starten

```
cd src
flask run
```
