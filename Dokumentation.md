# DBS-Projekt: Dokumentation des Arbeitsablaufs
## 28.06.2022
- Verstehen des Datensatzes
- Projektidee finden
- Schreiben eines Python-Skripts zu automatischen Abfrage von DOIs

## 01.07.2022
- Erstellen eines Git-Repositories
- Erstellen eines ER-Diagramms und einer README-Datei
- Erstellen einer Webapp die sich mit PostgeSQL verbinden kann

## 05.07.2022
- Datenbereinigung und aufbereitung
- SQL-Create-Befehle schreiben
- ER-Modell ändern

## 08.07.2022
- Erstellen einer Präsentation
  - Entscheidung: Präsentation in Markdown (+pandoc) oder in OnlyOffice erstellen?
    - parallele Änderungen in Markdown lässen sich leicht im git zusammen mergen
    - gleichzeitiges arbeiten an Office-Dokument geht nur über FUbox
    - Markdown (+pandoc) setzt voraus das jedes Gruppenmitglied pandoc und pdflatex installiert hat (um eine Vorschau der fertigen Präsentation ansehen zu können)
- Erstellen einer Dokumentation
- Suchfunktion für Web-Anwendung

## 15.07.2022
- Dokumentation überarbeiten

# Herausforderungen

Die größte Herausforderung war passende Daten um den Datensatz zu erweitern zu finden.
Nach längerer Suche haben wir einen selbst erstellten Datensatz, und einen Datensatz den wir auf GitHub finden konnten verwendet.
Unser selbst erstellter Datensatz enthält die Zuordnung von DOIs auf urls. Er wurde erstellt in dem wir automatisiert alle DOIs aufgerufen, und die URL auf die wir letztendlich umgeleitet wurden gespeichert haben.
Der Datensatz von GitHub enthält die Zuordnung von DOI-Präfixen zu Verlagen.

Eine weitere Herausforderung war die Analyse der Preise der verschiedenen Artikel (Im engeren Sinne also die Verfügbarkeit derer). 
Um diese zu ermitteln hatten wir überlegt automatisiert die Preise von den jeweiligen Webseiten abzufragen, da allerdings die Webseitenstruktur der URLs zu verschieden war schien es uns zu aufwendig den Preis jeweils automatisiert abzufragen. Für eine manuelle Abfrage waren es außerdem zu viele Artikel (mehr als 1000). Wir dachten uns, dass es je nach Verlag (den wir durch die DOIs mithilfe unseres zusätzlichen Datensatzes einfach zuordnen konnten) gewisse Preisklassen gäbe, diese waren allerdings auch nur sehr bedingt zutreffend, wenn es überhaupt welche gab. Somit haben wir uns entschieden die Preisanalyse auszulassen 

# Entscheidungen
> Themen der zusätzlichen Datensätze

Wir mussten uns entscheiden welche Daten den gegebenen Datensatz sinnvoll im Sinne unseres Projektes ergänzen. Wir haben uns aufgrund des Aufwandes dagegen entschieden die Preis-Daten miteinzubeziehen. 

> Programmiersprache und Web-Framework wählen

Wir haben Python und Flask als Programmierprache und Web-Framework gewählt, um es für alle Gruppenmitglieder unabhängig von der vorherigen Programmiererfahrung möglich zu machen code beizutragen.

> Wahl des Präsentationsformats

Wir hatten von Anfang an vor eine Folienpräsentation zu machen. Die Wahl der Software zur Folienerstellung fiel zwischen pandoc / beamer und OnlyOffice.
Pandoc stellte sich als zu kompliziert auf macOS zu installiren heraus, weswegen wir uns letztendlich dafür entschieden haben, OnlyOffice in der NextCloud der FU zu verwenden.
