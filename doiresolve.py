#!/usr/bin/env python3

import requests

from selenium import webdriver

driver = webdriver.Firefox()


def fetch_doi(doi: str):
    try:
        print("Resolving doi...")
        reply = requests.post("https://dx.doi.org", data=[("hdl", doi)])
        print(reply.url)
        print("Checking which website the url leads to...")
        driver.get(reply.url)
        file_name = doi.replace("/", "_")
        success  = driver.get_full_page_screenshot_as_file(f"Screenshots/{file_name}.png")
        assert(success)
        return driver.current_url
    except:
        return "unknown"


f = open("dois.txt", "r")
out = open("urls.txt", "a")
data = f.read()
lines = data.splitlines()
for doi in lines:
    print(doi)
    if doi != "":
        final_url = fetch_doi(doi)
        out.write(f"{doi},{final_url}\n")
