# save this as app.py
from flask import Flask, request, send_from_directory
from flask import render_template
import psycopg2
from matplotlib import pyplot as plt
from io import StringIO
import base64

database = conn = psycopg2.connect(
    host="localhost",
    database="literature",
    user="project",
    password="1234")

print("Connected to the database:", database)

app = Flask(__name__)

@app.route("/css/<path>")
def send_report(path):
    return send_from_directory("css", path)


@app.route("/publishers")
def publishers():
    cursor = conn.cursor()
    cursor.execute("SELECT COUNT(publisher.name), publisher.name " \
        "FROM research_literature_data " \
        "LEFT JOIN publisher ON research_literature_data.id_prefix = publisher.id_prefix " \
        "GROUP BY publisher.name")

    rows = cursor.fetchall()
    lists = list(zip(*rows))
    buffer = StringIO()
    fig = plt.figure(figsize = (15, 15))
    plt.pie(lists[0], labels = lists[1])
    fig.savefig(buffer, format="svg")
    return render_template("publishers.html", title="Publishers", image = buffer.getvalue())


@app.route("/")
def index():
    cursor = conn.cursor()
    if (request.args.get("q")):
        cursor.execute("SELECT research_literature_data.title, research_literature_data.author, publisher.name, url.url FROM research_literature_data " \
            "LEFT JOIN publisher ON research_literature_data.id_prefix = publisher.id_prefix " \
            "LEFT JOIN url ON research_literature_data.id_prefix = url.id_prefix AND research_literature_data.id = url.id " \
            "WHERE research_literature_data.title LIKE (%s)", ("%" + request.args.get("q") + "%",))
    else:
        cursor.execute("SELECT research_literature_data.title, research_literature_data.author, publisher.name, url.url FROM research_literature_data " \
            "LEFT JOIN publisher ON research_literature_data.id_prefix = publisher.id_prefix " \
            "LEFT JOIN url ON research_literature_data.id_prefix = url.id_prefix AND research_literature_data.id = url.id")

    rows = cursor.fetchall()
    return render_template('index.html', title="Literature", articles = rows, query = request.args.get("q"))
